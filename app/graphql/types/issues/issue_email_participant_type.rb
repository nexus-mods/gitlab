# frozen_string_literal: true

module Types
  module Issues
    class IssueEmailParticipantType < BaseObject
      authorize :ability

      field :email, type: GraphQL::Types::String, required: true,
      description: 'Email address for this participant.'
    end
  end
end
